import {AuthService} from './../../core/auth/auth.service';
import {StorageService} from './../../shared/services/storage.service';
import {CommonService} from './../../shared/services/common.service';
import {ChangeDetectorRef, Component, DoCheck, EventEmitter, Inject, OnDestroy, OnInit, Output} from '@angular/core';
import {DOCUMENT} from '@angular/common';
import {NavData, navItems} from '../../_nav';
import {TranslateService} from '@ngx-translate/core';
import {setCulture, setCurrencyCode} from '@syncfusion/ej2-base';
import {LoaderService} from '../../shared/services/loader.service';
import {ActivatedRoute, Router} from '@angular/router';
import {DASHBOARD_URL} from '../../shared/constant/root-url';
import { CURRENT_ROLE, CURRENT_ROLE_OID} from '../../shared/constant/storage-service-variables';

@Component({
  selector: 'app-dashboard',
  templateUrl: './default-layout.component.html'
})
export class DefaultLayoutComponent implements OnInit, OnDestroy, DoCheck {

  // public navItems = navItems;
  @Output() languageChangeEventEmitter = new EventEmitter<{}>() ;
  public navItems: NavData[];
  public menuJson: string;
  public roleOid: string;  locale: string;
  public sidebarMinimized = true;
  private changes: MutationObserver;
  public element: HTMLElement;
  public showLanguage: string;
  public LANGUAGE_BANGLA = 'বাংলা';
  public LANGUAGE_ENGLISH = 'English';
  public showLoader: boolean; currentRole: string;
  public roleList: Array<any>;
  constructor(private translate: TranslateService, private cdr: ChangeDetectorRef, private commonService: CommonService,
     private loaderService: LoaderService, private _router: Router, private storageService: StorageService,
              private _route: ActivatedRoute,
              private authService: AuthService,  @Inject(DOCUMENT) _document?: any) {

      this.changes = new MutationObserver((mutations) => {
        this.sidebarMinimized = _document.body.classList.contains('sidebar-minimized');
      });
      this.element = _document.body;
      this.changes.observe(<Element>this.element, {
        attributes: true,
        attributeFilter: ['class']
      });
      this.translate.addLangs(['bn', 'en']);
      this.translate.setDefaultLang('bn');
      this.showLanguage = this.LANGUAGE_ENGLISH;
      // const browserLang = this.translate.getBrowserLang();
      this.translate.use('bn');
      // this.translate.use(browserLang.match(/bn|en/) ? browserLang : 'bn');
  }

  getRoleList() {
    this.loaderService.display(true);
    this.commonService.getRoleList().subscribe(result => {
      this.loaderService.display(false);
        if (result != null) {
            this.roleList = result['body']['data'];
            this.roleOid = this.storageService.read(CURRENT_ROLE_OID);
            this.currentRole = this.storageService.read(CURRENT_ROLE);
            if (!this.roleOid && this.roleList) {
              this.roleList.forEach(element => {
                  if (element.defaultStatus === 'Yes') {
                    this.roleOid = element.oid;
                    this.currentRole = element.nameBn;
                    this.storageService.save(CURRENT_ROLE, element.nameBn);
                    this.storageService.save(CURRENT_ROLE_OID, element.oid);
                  }
              });
            }
            this.getMenuJsonData();
        }
    });
  }

  goToProfile() {

  }

  changeRole(role: any) {

    // ******************** uncomment this ********************************

    // if (role.nameBn === this.currentRole) {
    //   return;
    // }
    // this.roleOid = role.oid;
    // this.getMenuJsonData();
    // this.storageService.save(CURRENT_ROLE, role.nameBn);
    // this.storageService.save(CURRENT_ROLE_OID, role.oid);
    // this.currentRole = this.storageService.read(CURRENT_ROLE);
    // this._router.navigateByUrl('/dashboard');
  }

  getEmployeeInfo() {
    // this.commonService.getEmployeeBasicInfo().subscribe(result => {
    //     if (result != null) {
    //         this.employeeInfo = result['body']['data'];
    //     }
    // });

    // let currentUser: ICurrentUser ;
    // currentUser = JSON.parse(this.storageService.read(CURRENT_EMPLOYEE));
    // this.storageService.read(currentUser.employeeId).subscribe(result => {
    //   if (result != null) {
    //     this.employeeInfo = result['body']['data'];
    //   }
    // });



  }


  getMenuJsonData() {
    this.loaderService.display(true);
    this.commonService.getMenuJsonList(this.roleOid).subscribe(result => {
      this.getEmployeeInfo();
      this.loaderService.display(false);
        if (result != null) {
            this.menuJson = result['body']['data']['menuJson'];
            this.navItems = navItems;
        }
    });
  }

  ngOnInit(): void {
    this.navItems = navItems;

    this.loaderService.status.subscribe((val: boolean) => {
      this.showLoader = val;
    });
    // this.getRoleList();
    // this.currentRole = this.storageService.read(CURRENT_ROLE);
  }

  logout() {
    this.authService.logout();
  }
  changeLang() {
    if (this.translate.currentLang === 'bn') {
        this.translate.use('en');
        setCulture('en');
        setCurrencyCode('USD');
        this.locale = 'en';
        this.showLanguage = this.LANGUAGE_BANGLA;
      } else {
        this.translate.use('bn');
        setCulture('bn');
        setCurrencyCode('BDT');
        this.locale = 'bn';
        this.showLanguage = this.LANGUAGE_ENGLISH;
      }
  }

  ngOnDestroy(): void {
    this.changes.disconnect();
  }

  public ngDoCheck(): void {
    this.cdr.detectChanges();
  }

  backToDashboard() {
    window.location.replace(DASHBOARD_URL);
  }
}
