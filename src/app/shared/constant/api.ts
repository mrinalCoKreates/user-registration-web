import {
  CMN_ORGANOGRAM_URL,
  CMN_URL_TEMP,
  DEPLOYMENT_URL,
  HRM_CONFIGURATION_URL,
  HRM_PIM_URL,
  HRM_URL_TEMP,
  SEC_URL_TEMP, SSO_URL
} from './root-url';

export const SECURITY_URL = SEC_URL_TEMP;
export const COMMON_URL = CMN_URL_TEMP;
export const HRM_URL = HRM_URL_TEMP;

export const AUTH_TOKEN_URL = 'oauth/token';
export const AUTH_LOGIN_URL = 'master/authentication/v1/login';
export const AUTH_LOGOUT_URL = 'master/authentication/v1/logout';
export const LOGIN_URL = 'login';
export const LOGOUT_URL = 'logout';
export const UPLOAD_URL = 'api/v1/uploadAttachment';
export const DOWNLOAD_URL = 'api/v1/downloadFile';
export const GET_ROLE_LIST = 'master/role/v1/get-list-by-username';
export const GET_MENU_LIST = 'master/component/v1/get-list-by-role';
export const GET_EMP_BASIC = 'pim/employee/v1/get-basic-info-by-user-name';

export const GET_ITEM_LIST = 'master/item/v1/get-list';
export const GET_ITEM_CATEGORY_LIST = 'master/item/v1/get-category-list';
export const GET_ITEMLIST_BYCATEGORYOID = 'master/item/v1/get-itemlist-bycategoryoid';
export const GET_VENDOR_LIST = 'master/vendor/v1/get-list';
export const GET_FISCAL_YEAR_LIST = 'master/fiscal-year/v1/get-list';

export const CREATE_URL = 'create';
export const UPDATE_URL = 'update';
export const DELETE_URL = 'delete';
export const GET_LIST_URL = 'get-list';
export const GET_MINISTRY_OR_DIVISION_LIST_PATH = 'get-ministry-or-division-list';
export const GET_BY_OID_URL = 'get-by-oid';
export const GET_LIST_BY_OID_SET_URL = 'get-list-by-oid-set';
export const GET_BY_EMPLOYEE_OID_AND_OFFICE_OID_PATH = 'get-by-employee-oid-and-office-oid';
export const EMPLOYEE_REMOVE_FROM_OFFICE_UNITPOST = 'remove-employee-from-officeunitpost';

export const SUMMARY_PATH = 'summary/';

export const GET_LIST_BY_FILTER_URL = 'get-by-filter';
export const GET_BY_EMPLOYEE_URL = 'get-by-employee-oid';
export const GET_ACTIVE_BY_EMPLOYEE_URL = 'get-active-by-employee-oid';
export const GET_HISTORY_BY_EMPLOYEE_OID_PATH = 'get-history-by-employee-oid';

/**************************** <SECURITY> ***************************/

export const RETURN_BASE_URL = SSO_URL + LOGIN_URL + '?returnUrl=' + DEPLOYMENT_URL ;


/**************************** <HRM-PIM> ****************************/

export const USER_CURRENT_VERSION = 'v1';
export const USER_URL = `${HRM_PIM_URL}user/${USER_CURRENT_VERSION}/`;

export const EMPLOYEE_CURRENT_VERSION = 'v1';
export const EMPLOYEE_URL = `${HRM_PIM_URL}employee/${EMPLOYEE_CURRENT_VERSION}/`;

export const BATCH_CURRENT_VERSION = 'v1';
export const BATCH_URL = `${HRM_PIM_URL}batch/${BATCH_CURRENT_VERSION}/`;

export const CADRE_CURRENT_VERSION = 'v1';
export const CADRE_URL = `${HRM_PIM_URL}cadre/${CADRE_CURRENT_VERSION}/`;

export const LANGUAGE_CURRENT_VERSION = 'v1';
export const LANGUAGE_URL = `${HRM_PIM_URL}language/${LANGUAGE_CURRENT_VERSION}/`;

export const GRADE_CURRENT_VERSION = 'v1';
export const GRADE_URL = `${HRM_PIM_URL}grade/${GRADE_CURRENT_VERSION}/`;

export const EMPLOYMENT_TYPE_CURRENT_VERSION = 'v1';
export const EMPLOYMENT_TYPE_URL = `${HRM_PIM_URL}employment-type/${EMPLOYMENT_TYPE_CURRENT_VERSION}/`;

export const EMPLOYEE_FOREIGN_TRAVEL_CURRENT_VERSION = 'v1';
export const EMPLOYEE_FOREIGN_TRAVEL_URL = `${HRM_PIM_URL}employee-foreign-travel/${EMPLOYEE_FOREIGN_TRAVEL_CURRENT_VERSION}/`;


export const EMPLOYEE_AWARD_CURRENT_VERSION = 'v1';
export const EMPLOYEE_AWARD_URL = `${HRM_PIM_URL}employee-award/${EMPLOYEE_AWARD_CURRENT_VERSION}/`;

export const EMPLOYEE_CHILDREN_INFO_CURRENT_VERSION = 'v1';
export const EMPLOYEE_CHILDREN_INFO_URL = `${HRM_PIM_URL}employee-children-info/${EMPLOYEE_CHILDREN_INFO_CURRENT_VERSION}/`;


export const EMPLOYEE_CONFIRMATION_CURRENT_VERSION = 'v1';
export const EMPLOYEE_CONFIRMATION_URL = `${HRM_PIM_URL}employee-confirmation/${EMPLOYEE_CONFIRMATION_CURRENT_VERSION}/`;


export const EMPLOYEE_DISCIPLINARY_ACTION_CURRENT_VERSION = 'v1';
export const EMPLOYEE_DISCIPLINARY_ACTION_URL = `${HRM_PIM_URL}employee-disciplinary-action/${EMPLOYEE_DISCIPLINARY_ACTION_CURRENT_VERSION}/`;

export const EMPLOYEE_OTHER_SERVICES_CURRENT_VERSION = 'v1';
export const EMPLOYEE_OTHER_SERVICES_URL = `${HRM_PIM_URL}employee-other-services/${EMPLOYEE_OTHER_SERVICES_CURRENT_VERSION}/`;


export const EMPLOYEE_EDUCATION_QUALIFICATION_CURRENT_VERSION = 'v1';
export const EMPLOYEE_EDUCATION_QUALIFICATION_URL = `${HRM_PIM_URL}employee-education-qualification/${EMPLOYEE_EDUCATION_QUALIFICATION_CURRENT_VERSION}/`;

export const ADDRESS_INFO_CURRENT_VERSION = 'v1';
export const ADDRESS_INFO_URL = `${HRM_PIM_URL}address-info/${ADDRESS_INFO_CURRENT_VERSION}/`;

export const EMPLOYEE_LANGUAGE_SKILL_CURRENT_VERSION = 'v1';
export const EMPLOYEE_LANGUAGE_SKILL_URL = `${HRM_PIM_URL}employee-language-skill/${EMPLOYEE_LANGUAGE_SKILL_CURRENT_VERSION}/`;

export const EMPLOYEE_MASTER_INFO_CURRENT_VERSION = 'v1';
export const EMPLOYEE_MASTER_INFO_URL = `${HRM_PIM_URL}employee-master-info/${EMPLOYEE_MASTER_INFO_CURRENT_VERSION}/`;

export const EMPLOYEE_OFFICE_CURRENT_VERSION = 'v1';
export const EMPLOYEE_OFFICE_URL = `${HRM_PIM_URL}employee-office/${EMPLOYEE_OFFICE_CURRENT_VERSION}/`;

export const EMPLOYEE_ADDITIONAL_PROF_QUALIFICATION_CURRENT_VERSION = 'v1';
export const EMPLOYEE_ADDITIONAL_PROF_QUALIFICATION_URL = `${HRM_PIM_URL}employee-additional-prof-qualification/${EMPLOYEE_ADDITIONAL_PROF_QUALIFICATION_CURRENT_VERSION}/`;



export const EMPLOYEE_PERSONAL_INFO_CURRENT_VERSION = 'v1';
export const EMPLOYEE_PERSONAL_INFO_URL = `${HRM_PIM_URL}employee-personal-info/${EMPLOYEE_PERSONAL_INFO_CURRENT_VERSION}/`;

export const EMPLOYEE_TRAINING_INFO_CURRENT_VERSION = 'v1';
export const EMPLOYEE_TRAINING_INFO_URL = `${HRM_PIM_URL}employee-training-info/${EMPLOYEE_TRAINING_INFO_CURRENT_VERSION}/`;


export const EMPLOYEE_PUBLICATION_CURRENT_VERSION = 'v1';
export const EMPLOYEE_PUBLICATION_URL = `${HRM_PIM_URL}employee-publication/${EMPLOYEE_PUBLICATION_CURRENT_VERSION}/`;

export const EMPLOYEE_MEDICAL_INFO_CURRENT_VERSION = 'v1';
export const EMPLOYEE_MEDICAL_INFO_URL = `${HRM_PIM_URL}employee-medical-info/${EMPLOYEE_MEDICAL_INFO_CURRENT_VERSION}/`;

export const EMPLOYEE_PROBATION_CURRENT_VERSION = 'v1';
export const EMPLOYEE_PROBATION_URL = `${HRM_PIM_URL}employee-probation/${EMPLOYEE_PROBATION_CURRENT_VERSION}/`;

export const EMPLOYEE_SPOUSE_INFO_CURRENT_VERSION = 'v1';
export const EMPLOYEE_SPOUSE_INFO_URL = `${HRM_PIM_URL}employee-spouse-info/${EMPLOYEE_SPOUSE_INFO_CURRENT_VERSION}/`;




/**************************** </HRM-CONFIG> ****************************/

export const HOLIDAY_CURRENT_VERSION = 'v1';
export const HOLIDAY_URL = `${HRM_CONFIGURATION_URL}holiday/${HOLIDAY_CURRENT_VERSION}/`;
export const GET_LIST_BY_YEAR_URL = 'get-list-by-year';


/**************************** <HRM-CONFIG> ****************************/





/**************************** </HRM-PIM> ****************************/

/**************************** <CMN-ORGANOGRAM> ****************************/

export const GET_BY_OFFICE_URL = 'get-by-office-oid';

export const GET_SUMMARY_BY_OFFICE_UNIT_URL = 'get-summary-by-office-unit-oid';

export const OFFICE_CURRENT_VERSION = 'v1';
export const OFFICE_URL = `${CMN_ORGANOGRAM_URL}office/${OFFICE_CURRENT_VERSION}/`;

export const GET_BY_ROOT_AND_LAYER_URL = 'get-by-root-and-layer';
export const GET_ROOTS_URL = 'get-roots';

export const OFFICE_LAYER_CURRENT_VERSION = 'v1';
export const OFFICE_LAYER_URL = `${CMN_ORGANOGRAM_URL}office-layer/${OFFICE_LAYER_CURRENT_VERSION}/`;

export const OFFICE_UNIT_POST_CURRENT_VERSION = 'v1';
export const OFFICE_UNIT_POST_URL = `${CMN_ORGANOGRAM_URL}office-unit-post/${OFFICE_UNIT_POST_CURRENT_VERSION}/`;

export const OFFICE_UNIT_CURRENT_VERSION = 'v1';
export const OFFICE_UNIT_URL = `${CMN_ORGANOGRAM_URL}office-unit/${OFFICE_UNIT_CURRENT_VERSION}/`;

/**************************** </CMN-ORGANOGRAM> ****************************/
