import {FormGroup, ValidationErrors, ValidatorFn} from '@angular/forms';

export const registrationValidator: ValidatorFn = (control: FormGroup): ValidationErrors | null => {
  if (control.get('mobileNo').value) {
    const mobileNo = control.get('mobileNo').value.toString();
    const mobileNoLength = mobileNo.length;
    for (let i = 0; i < mobileNoLength; i++) {
      if (mobileNo.charAt(i) < '0' || mobileNo.charAt(i) > '9') {
        return {'formValidationError' : true, 'message': 'Mobile number cannot contain anything except digits'};
      }
    }
    if (mobileNoLength > 11) {
      return {'formValidationError' : true, 'message': 'Mobile number cannot have more than 11 digits'};
    }
  }
  if (control.get('password').value && control.get('rePassword').value) {
    const password = control.get('password').value.toString();
    const rePassword = control.get('rePassword').value.toString();
    if (rePassword.length < password.length) {
        return {'formValidationError' : true, 'message': 'Passwords length did not match yet...'};
    }
    if (rePassword.length >= password.length) {
      if (password !== rePassword) {
        return {'formValidationError' : true, 'message': 'Passwords did not match !!!'};
      }
    }
    return null;
  }
};


