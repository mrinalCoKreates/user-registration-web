import { GET_FISCAL_YEAR_LIST, GET_MENU_LIST, HRM_URL, GET_EMP_BASIC } from './../constant/api';
import { Injectable } from '@angular/core';
import { SearchParams } from '../model/request/search-params';
import { Observable } from 'rxjs/internal/Observable';
import { RequestList } from '../model/request/request-list';
import { GET_VENDOR_LIST, GET_ITEM_LIST, COMMON_URL, GET_ROLE_LIST, GET_ITEM_CATEGORY_LIST, GET_ITEMLIST_BYCATEGORYOID, SECURITY_URL } from '../constant/api';
import { RestClient } from './rest-client';
import { RequestByOid } from '../model/request/request-by-oid';
import { RequestMenu } from '../model/request/request-menu';


@Injectable({
    providedIn: 'root'
})
export class CommonService {

    constructor(private restClient: RestClient) {}

    getItemCategoryList(params: SearchParams): Observable<any> {
        const requestParams = new RequestList();
        requestParams.header.requestType = GET_ITEM_CATEGORY_LIST;
        requestParams.header.requestSourceService = 'get-list';
        requestParams.body.searchParams = params;
        return this.restClient.post(COMMON_URL + GET_ITEM_CATEGORY_LIST, requestParams);
    }

    getFiscalYears(): Observable<any> {
        const requestParams = new RequestList();
        requestParams.header.requestType = GET_FISCAL_YEAR_LIST;
        requestParams.header.requestSourceService = 'get-list';
        requestParams.body.searchParams = new SearchParams();
        return this.restClient.post(COMMON_URL + GET_FISCAL_YEAR_LIST, requestParams);
      }

    getItemListByCategory( categoryOid: string): Observable<any> {
        const requestParams = new RequestByOid();
        requestParams.header.requestType = GET_ITEMLIST_BYCATEGORYOID;
        requestParams.header.requestSourceService = 'get-itemlist-bycategoryoid';
        requestParams.body.oid = categoryOid;
        return this.restClient.post(COMMON_URL + GET_ITEMLIST_BYCATEGORYOID, requestParams);
    }

    getItemList(params: SearchParams): Observable<any> {
        const requestParams = new RequestList();
        requestParams.header.requestType = GET_ITEM_LIST;
        requestParams.header.requestSourceService = 'get-list';
        requestParams.body.searchParams = params;
        return this.restClient.post(COMMON_URL + GET_ITEM_LIST, requestParams);
    }

    getVendorList(params: SearchParams): Observable<any> {
        const requestParams = new RequestList();
        requestParams.header.requestType = GET_VENDOR_LIST;
        requestParams.header.requestSourceService = 'get-list';
        requestParams.body.searchParams = params;
        return this.restClient.post(COMMON_URL + GET_VENDOR_LIST, requestParams);
    }

    getRoleList(): Observable<any> {
        const requestParams = new RequestList();
        requestParams.header.requestType = GET_ROLE_LIST;
        requestParams.header.requestSourceService = 'get-list';
        requestParams.body.operation = 'GetList';
        return this.restClient.post(SECURITY_URL + GET_ROLE_LIST, requestParams);
    }

    getMenuJsonList(roleOid: string): Observable<any> {
        const requestParams = new RequestMenu();
        requestParams.header.requestType = GET_MENU_LIST;
        requestParams.header.requestSourceService = 'get-list-by-role';
        requestParams.body.roleOid = roleOid;
        return this.restClient.post(SECURITY_URL + GET_MENU_LIST, requestParams);
    }

    getEmployeeBasicInfo(): Observable<any> {
        const requestParams = new RequestList();
        requestParams.header.requestType = GET_EMP_BASIC;
        requestParams.header.requestSourceService = 'get-basic-info-by-user-name';
        requestParams.body.operation = 'get-basic-info-by-user-name';
        return this.restClient.post(HRM_URL + GET_EMP_BASIC, requestParams);
    }
}
