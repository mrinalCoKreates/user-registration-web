import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-hrm',
  template: `
      <router-outlet></router-outlet>
      <div id="ej2Toast" #toast></div>
  `
})
export class UserContainerComponent implements OnInit {

  constructor() {
  }

  ngOnInit() {
  }

}
