import {CrudRequestService} from './crud-request.service';
import {Injectable} from '@angular/core';
import {User} from '../model/user';
import {RestClient} from '../../../../shared/services/rest-client';
import {USER_CURRENT_VERSION, USER_URL} from '../../../../shared/constant/api';

@Injectable()
export class UserService extends CrudRequestService<User> {

  constructor(client: RestClient) {
    super(client, USER_URL, USER_CURRENT_VERSION);
  }

}
