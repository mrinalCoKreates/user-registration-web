import {ElementRef, Injectable} from '@angular/core';
import {ToastService} from '../../../../shared/services/toast.service';

@Injectable()
export class ToasterHelper {
  constructor(private toastService: ToastService) {}
  showToasMessage(status: string, message: string, toast: ElementRef) {
    if (status === '200') {
      this.toastService.showSuccessToast(toast.nativeElement, {
        content: message,
      });
    } else {
      this.toastService.showErrorToast(toast.nativeElement, {
        content: message
      });
    }
  }
}
