import {Observable} from 'rxjs/Observable';

import * as uuid from 'uuid';
import {IRequestBody, IRequestHeader} from '../model/request';
import {IResponse, IResponseData} from '../model/response';
import {RestClient} from '../../../../shared/services/rest-client';
import {CREATE_URL } from '../../../../shared/constant/api';

export abstract class CrudRequestService<I extends IRequestBody & IResponseData> {

  protected constructor(private _restClient: RestClient,
                        protected _BASE_URL: string,
                        protected _CURRENT_VERSION: string) {
  }


  create(i: I): Observable<IResponse<I>> {
    return this.sendRequest<I>(this._BASE_URL + CREATE_URL, CREATE_URL, this._CURRENT_VERSION, i);
  }

  // getList(): Observable<IResponse<I>> {
  //   return this.sendRequest<IRequestBody>(this._BASE_URL + GET_LIST_URL, GET_LIST_URL, this._CURRENT_VERSION, {});
  // }
  //
  // getByOid(oid: string): Observable<IResponse<I>> {
  //   return this.sendRequest<IOidHolderRequestBody>(this._BASE_URL + GET_BY_OID_URL, GET_BY_OID_URL, this._CURRENT_VERSION, {oid: oid});
  // }
  //
  // getByOidSet(oids: string[]): Observable<IResponse<I>> {
  //   return this.sendRequest<IGetListByOidSetHolderRequestBody>(this._BASE_URL + GET_LIST_BY_OID_SET_URL, GET_LIST_BY_OID_SET_URL, this._CURRENT_VERSION, {oids: oids});
  // }
  //
  // update(i: I): Observable<IResponse<I>> {
  //   return this.sendRequest<I>(this._BASE_URL + UPDATE_URL, UPDATE_URL, this._CURRENT_VERSION, i);
  // }
  //
  // delete(oid: string): Observable<IResponse<I>> {
  //   return this.sendRequest<IOidHolderRequestBody>(this._BASE_URL + DELETE_URL, DELETE_URL, this._CURRENT_VERSION, {oid: oid});
  // }

  protected sendRequest<B extends IRequestBody>
  (url: string,
   sourceService: string,
   version: string,
   body: B,
   meta?: Object): Observable<IResponse<I>> {
    return this._restClient.post(url,
      {
        header: this._createHeader(url, sourceService, version),
        meta: meta || {},
        body: body
      });
  }

  protected _createHeader(type: string, sourceService: string, version: string): IRequestHeader {
    return {
      requestId: uuid.v4(),
      requestSource: 'portal',
      requestSourceService: sourceService,
      requestClient: 'grp',
      requestType: type,
      requestTime: new Date().toISOString(),
      requestVersion: version,
      requestTimeoutInSeconds: 30,
      requestRetryCount: 3
    };
  }
}
