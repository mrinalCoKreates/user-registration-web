export interface IRequestHeader {
  requestId: string;
  requestSource: string;
  requestSourceService: string;
  requestClient: string;
  requestType: string;
  requestTime: string;
  requestVersion: string;
  requestTimeoutInSeconds: number;
  requestRetryCount: number;
}

// tslint:disable-next-line:no-empty-interface
export interface IRequestBody {
}

export interface IOidHolderRequestBody {
  oid: string;
}

export interface IMultipleOidHolderRequestBody {
  employeeMasterInfoOid?: string;
  officeOid: string;
}

export interface IGetListByOidSetHolderRequestBody {
  oids: string[];
  strict?: string;
}

export interface IRequest<T extends IRequestBody> {
  header: IRequestHeader;
  meta: Object;
  body: T;
}
