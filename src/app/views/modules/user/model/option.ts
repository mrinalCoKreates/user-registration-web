export interface IOption {
  name: string;
  nameBn?: string;
  value?: string;
}
