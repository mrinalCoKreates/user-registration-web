
export interface User {
  nameEn: string;
  nameBn: string;
  mobileNo?: string;
  email: string;
  gender?: string;
  address?: string;
  isEmployee?: string;
  status?: string;
  userType?: string;
  password: string;
}
