import {IOption} from './option';
import {Observable} from 'rxjs/Observable';

export interface IFieldDescription {
  name: string;
  field: string;
  nameBn?: string;
  dataLoader?: (selections: Record<IFieldDescription['field'], string>) => Observable<IOption[]>;
}

export interface IButtonDescription {
  color?: 'primary' | 'accent' | 'warn'; // Defaults to 'primary'
  text?: string;
  icon: string; // MAT-ICON !!NOT!! FAB-ICON
  listener: (data?: any) => any;
  disabled?: boolean;
}

export interface IListDescription {
  fields: IFieldDescription[];
  searchFields?: IFieldDescription[];
  dataLoader: (page: number, size: number, searchText?: string) => Observable<any>;
  topButton?: IButtonDescription;
  buttons?: IButtonDescription[];
}

interface IBasicFormControl extends IFieldDescription {
  required: boolean;
  readonly?: boolean;
  customError?: string;
  customErrorText?: string;
}

export interface ITextFormControl extends IBasicFormControl {
  type: 'text';
  dataLoader?: () => Observable<IOption[]>;
}

export interface IPasswordFormControl extends IBasicFormControl {
  type: 'password';
  dataLoader?: () => Observable<IOption[]>;
}

export interface IDateFormControl extends IBasicFormControl {
  type: 'date';
}

export interface INumberFormControl extends IBasicFormControl {
  type: 'number';
}

export interface IReflectFormControl extends IBasicFormControl {
  type: 'reflect';
  parentField: string;
  converter: (year: string) => string;
}

export interface IBooleanFormControl extends IBasicFormControl {
  type: 'boolean';
  color?: 'primary' | 'accent' | 'warn';
  defaultValue?: boolean;
  options?: {'trueValue': string, 'falseValue': string};
}

export interface ISelectFormControl extends IBasicFormControl {
  type: 'select';
  dataLoader?: () => Observable<IOption[]>;
}

export interface IRadioFormControl extends IBasicFormControl {
  type: 'radio';
  dataLoader?: () => Observable<IOption[]>;
}

export interface ISliderFormControl extends IBasicFormControl {
  type: 'slider';
}

export interface ICheckBoxFormControl extends IBasicFormControl {
  type: 'checkBox';
}

export interface IDependantSelectFormControl extends IBasicFormControl {
  type: 'dependant-select';
  parents: IFieldDescription['field'][];
  dataLoader: (selections: Record<IFieldDescription['field'], string>) => Observable<IOption[]>;
}

export type IFormControl =
  ITextFormControl
  | IPasswordFormControl
  | IDateFormControl
  | INumberFormControl
  | IBooleanFormControl
  | ISelectFormControl
  | IRadioFormControl
  | IDependantSelectFormControl
  | ISliderFormControl
  | ICheckBoxFormControl
  | IReflectFormControl;

