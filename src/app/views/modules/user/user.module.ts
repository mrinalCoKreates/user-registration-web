import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ReactiveFormsModule} from '@angular/forms';
import {TranslateModule} from '@ngx-translate/core';
import {UserRoutingModule} from './user-routing.module';
import {UserContainerComponent} from './components/user-container/user-container.component';
import {ToasterHelper} from './service/toaster.helper';


@NgModule({
  declarations: [UserContainerComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    UserRoutingModule,
    TranslateModule
  ],
  providers: [
    ToasterHelper
  ],
})
export class UserModule { }
