import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {RegistrationContainerComponent} from './components/registration-container/registration-container.component';
import {RegisterComponent} from './register/register.component';

const routes: Routes = [
  {
    path: '',
    data: { title: 'Registration TopMenu' },
    component: RegistrationContainerComponent,
    children: [
      {
        path: '',
        data: { title: 'Register' },
        component: RegisterComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RegistrationRoutingModule { }
