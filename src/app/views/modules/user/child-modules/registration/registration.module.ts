import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RegistrationContainerComponent} from './components/registration-container/registration-container.component';
import {RegistrationRoutingModule} from './registration-routing.module';
import {RegisterComponent} from './register/register.component';
import {MatDatepickerModule, MatRadioModule, MatSelectModule, MatSlideToggleModule} from '@angular/material';
import {TranslateModule} from '@ngx-translate/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MaterialModule} from '../../../../../material.module';
import { MatCheckboxModule  } from '@angular/material';
import {UserService} from '../../service/user.service';
import { ProfileComponent } from './profile/profile.component';


@NgModule({
  declarations: [RegistrationContainerComponent, RegisterComponent, ProfileComponent],
  imports: [
    CommonModule,
    RegistrationRoutingModule,
    MatSelectModule,
    TranslateModule,
    MatDatepickerModule,
    MatSlideToggleModule,
    ReactiveFormsModule,
    MaterialModule,
    MatRadioModule,
    FormsModule,
    MatCheckboxModule
  ],
  providers: [
    UserService
  ],
})
export class RegistrationModule { }
