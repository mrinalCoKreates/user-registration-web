import { Component, OnInit } from '@angular/core';
import {AbstractControl, FormControl, FormGroup, Validators} from '@angular/forms';
import {registrationValidator} from '../../../../../../shared/validators/registration-validator.directive';
import {IFormControl} from '../../../model/form-control';
import {UserService} from '../../../service/user.service';
import {Router} from '@angular/router';
import {User} from '../../../model/user';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  _data: Record<string, any[]> = {};
  _formGroup: FormGroup;
  _formControl: IFormControl[];
  formName: string;
  constructor(private userService: UserService, private _router: Router) { }

  ngOnInit() {
    this._data['gender'] = [
      { name: 'Male', value: 'Male'},
      { name: 'Female', value: 'Female'},
      { name: 'Other', value: 'Other'},
    ];
    this._data['userType'] = [
      { name: 'Super Admin', value: 'Super Admin'},
      { name: 'Admin', value: 'Admin'},
      { name: 'Normal', value: 'Normal'},
    ];
    this._formGroup = new FormGroup({
        nameEn: new FormControl('', [Validators.required]),
        nameBn: new FormControl('', [Validators.required]),
        mobileNo: new FormControl(''),
        email: new FormControl('', [Validators.email]),
        gender: new FormControl(''),
        address: new FormControl(''),
        isEmployee: new FormControl(''),
        status: new FormControl(''),
        userType: new FormControl(''),
        password: new FormControl('', [Validators.required]),
        rePassword: new FormControl('', [Validators.required]),
      },
      {validators: registrationValidator});
    this.formName = 'Registration' ;
    this._formControl = [
      {required: true, type: 'text',   name: 'Name (English)', field: 'nameEn'},
      {required: true, type: 'text',  name: 'Name (Bangla)', field: 'nameBn'},
      {required: false, type: 'number',  name: 'Mobile No', field: 'mobileNo'},
      {required: true, type: 'text',  name: 'Email', field: 'email'},
      {required: false, type: 'select',  name: 'Gender', field: 'gender'},
      {required: true, type: 'text',  name: 'Address', field: 'address'},
      {required: false, type: 'boolean', options: {'trueValue': 'Yes', 'falseValue': 'No'}, name: 'Is Employee?', field: 'isEmployee'},
      {required: false, type: 'checkBox', name: 'Status', field: 'status'},
      {required: false, type: 'radio',  name: 'User Type', field: 'userType'},
      {required: true, type: 'password',  name: 'Password', field: 'password'},
      {required: true, type: 'password',  name: 'Retype Password', field: 'rePassword'},
    ];

  }

  isErrorState(control: AbstractControl): boolean {
    return control.invalid && (control.dirty || control.touched);
  }

  onSubmit() {
    this.userService
      .create(this.parse(this._formGroup.value))
      .subscribe(response => {
        // this._toasterHelper.showToasMessage('200', EMPLOYEE_CREATED_SUCCESSFULLY, this.toast);
        console.log(response.body.data[0]);
        this._router.navigate([`user/confirmation`]);
      });
  }
  parse(formValue: User) {
     formValue.status ? formValue.status = 'Active' : formValue.status = 'Inactive' ;
     formValue.gender === '' ? formValue.gender = null : formValue.gender = formValue.gender;
    formValue.isEmployee ? formValue.isEmployee = 'Yes' : formValue.isEmployee = 'No' ;
    console.log(formValue);
    return formValue;
  }
}
