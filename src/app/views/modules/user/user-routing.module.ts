import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {UserContainerComponent} from './components/user-container/user-container.component';

const routes: Routes = [
  {
    path: '',
    data: { title: 'User TopMenu' },
    component: UserContainerComponent,
    children: [
      {
        path: 'registration',
        loadChildren: () => import('./child-modules/registration/registration.module').then(m => m.RegistrationModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
