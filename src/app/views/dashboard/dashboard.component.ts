import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  templateUrl: 'dashboard.component.html',
  styleUrls: ['dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

    message;
    constructor(private _router: Router) {

     }

     addNew() {
      this._router.navigate(['asset/commissioning-acquisition/temp-item/add-temp-item']);
     }

     goToItemList() {
        this._router.navigate(['asset/commissioning-acquisition/temp-item/temp-item-data-entry-operator']);
     }

    ngOnInit() {

    }
}

