import {NgModule} from '@angular/core';
import {
  AgendaService,
  DayService,
  ExcelExportService,
  MonthAgendaService,
  MonthService,
  ResizeService,
  ScheduleModule,
  TimelineMonthService,
  TimelineViewsService,
  WeekService,
  WorkWeekService
} from '@syncfusion/ej2-angular-schedule';
import {
  ColumnMenuService,
  CommandColumnService,
  ContextMenuService,
  DetailRowService,
  EditService,
  FilterService,
  GridModule,
  GroupService,
  PagerModule,
  PageService,
  PdfExportService,
  SortService,
  ToolbarService
} from '@syncfusion/ej2-angular-grids';
import {DialogModule} from '@syncfusion/ej2-angular-popups';
import {ToastModule} from '@syncfusion/ej2-angular-notifications';
import {TabModule} from '@syncfusion/ej2-angular-navigations';
import {DropDownListModule} from '@syncfusion/ej2-angular-dropdowns';

@NgModule({
  exports: [ScheduleModule, GridModule, PagerModule, DialogModule, ToastModule, TabModule, DropDownListModule],
  providers: [DayService, WeekService, WorkWeekService, MonthService, AgendaService, MonthAgendaService, TimelineViewsService, TimelineMonthService,
    PageService, ResizeService, GroupService, SortService, FilterService, ColumnMenuService, EditService, ExcelExportService, PdfExportService, ContextMenuService,
    ToolbarService, CommandColumnService, DetailRowService]
})
export class SyncfusionModule {
}
