import {AUTH_LOGIN_URL, LOGOUT_URL} from './../../shared/constant/api';
import {StorageService} from './../../shared/services/storage.service';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {SECURITY_URL} from '../../shared/constant/api';
import {ICurrentUser} from '../../shared/model/model/current-user';
import {BehaviorSubject} from 'rxjs/internal/BehaviorSubject';
import {Observable} from 'rxjs/internal/Observable';
import {map} from 'rxjs/internal/operators/map';
import {RequestList} from '../../shared/model/request/request-list';
import {Router} from '@angular/router';
import {pipe} from 'rxjs/internal-compatibility';
import {SSO_URL} from '../../shared/constant/root-url';
import {CURRENT_USER} from '../../shared/constant/storage-service-variables';

@Injectable({ providedIn: 'root'})
export class AuthService {

  public currentUser: Observable<ICurrentUser>;
  private currentUserSubject: BehaviorSubject<ICurrentUser>;
    private refresh_token: string;
    constructor(private http: HttpClient, private storageService: StorageService, private _router: Router) {
      if (this.storageService.read(CURRENT_USER)) {
        this.currentUserSubject = new BehaviorSubject<ICurrentUser>(this.storageService.read(CURRENT_USER));
        this.currentUser = this.currentUserSubject.asObservable();
      }
    }
  public get currentUserValue(): ICurrentUser {
    if (this.currentUserSubject) {
      return this.currentUserSubject.value;
    }
    return null;
  }

    login(username: string, password: string) {
        let creds:  Record<string, string>;
        creds = {};
        creds['userId'] = username ;
        creds['userPassword'] = password ;
        creds['clientId'] = 'grp-web-portal' ;
        creds['clientPassword'] = '123456' ;
        creds['grantType'] = 'password' ;
        let headers = new HttpHeaders();
        headers = headers.append('Content-Type', 'application/json;charset=UTF-8');
        return this.http.post<any>(SECURITY_URL + AUTH_LOGIN_URL, creds , { headers: headers})
            .pipe(map(user => {
                this.storageService.save(CURRENT_USER, user);
                this.currentUserSubject = new BehaviorSubject<ICurrentUser>(this.storageService.read(CURRENT_USER));
                this.currentUser = this.currentUserSubject.asObservable();
                this.currentUserSubject.next(user);
                return user;
        }));
    }

    logout() {
        // const requestParams = new RequestList();
        // requestParams.header.requestType = LOGOUT_URL;
        // requestParams.header.requestSourceService = 'logout';
        // requestParams.body.operation = 'logout';
        // requestParams.t

      // this.currentUserSubject.next(null);

      this.storageService.clear();
      window.location.assign(SSO_URL + LOGOUT_URL);

      // ******************** hit logout api directly ******************************

        // let creds:  Record<string, string>;
        // creds = {};
        // this.currentUser.subscribe(pipe(currentUser => {
        //   creds[TOKEN] = currentUser['access_token'];
        //     let headers = new HttpHeaders();
        //   headers = headers.append('Content-Type', 'application/json');
        //   this.http.post(SECURITY_URL + LOGOUT_URL, creds , { headers: headers})
        //     .subscribe(user => {
        //       this.storageService.clear();
        //       this.currentUserSubject.next(null);
        //       window.location.assign('http://45.79.102.237:94/grp-sso/logout');
        //     });
        // }));
    }

    logoutWithoutToken() {
        /*this.storageService.clear();
        this.currentUserSubject.next(null);
        this._router.navigateByUrl('/login');
        return throwError('User not authorized, try again!!');*/
        return;
    }

    // refreshToken(): Observable<ICurrentUser> {
    //     const currentUser = this.currentUserValue;
    //     if (currentUser && currentUser.refresh_token) {
    //         this.refresh_token = currentUser.refresh_token;
    //     }
    //     const creds = 'token=' + this.refresh_token + '&grant_type=refresh_token';
    //     return this.http.post<ICurrentUser>(SECURITY_URL + AUTH_LOGIN_URL, creds)
    //       .pipe(map(user => {
    //           if (user) {
    //             this.storageService.save(CURRENT_USER, JSON.stringify(user));
    //           }
    //           return <ICurrentUser>user;
    //     }));
    // }
}
