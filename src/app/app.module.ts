import {DetailRowService} from '@syncfusion/ej2-angular-grids';
import {FormsModule} from '@angular/forms';
import {BrowserModule} from '@angular/platform-browser';
import {ErrorHandler, NgModule} from '@angular/core';
import {APP_BASE_HREF} from '@angular/common';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {PerfectScrollbarModule} from 'ngx-perfect-scrollbar';
import {AppComponent} from './app.component';
// Import containers
import {DefaultLayoutComponent} from './containers';
import {P404Component} from './views/error/404.component';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {AppAsideModule, AppFooterModule, AppHeaderModule} from '@coreui/angular';
// Import routing module
import {AppRoutingModule} from './app.routing';
// Import 3rd party components
import {HTTP_INTERCEPTORS, HttpClient, HttpClientModule} from '@angular/common/http';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {HrefPreventDefaultDirective} from './core/directives/href-prevent-default.directive';
import {environment} from '../environments/environment';
import {ServiceWorkerModule} from '@angular/service-worker';
import {MAT_DATE_LOCALE, MatSnackBarModule} from '@angular/material';
import {AppSidebarModule} from './core/sidebar';
import {AppBreadcrumbModule} from './core/breadcrumb';
import {FieldErrorDisplayComponent} from './shared/templates/field-error-display.component';
import {ChartsModule} from 'ng2-charts';
import {HttpErrorInterceptor} from './shared/services/http-error.interceptor';
import {GlobalErrorHandler} from './shared/services/global-error-handler';
import {SlimLoadingBarModule} from 'ng2-slim-loading-bar';
import {LoadingInterceptor} from './shared/services/loading.interceptor';

const APP_CONTAINERS = [
  DefaultLayoutComponent
];

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    ServiceWorkerModule.register('./ngsw-worker.js', { enabled: environment.production }),
    FormsModule,
    AppRoutingModule,
    AppAsideModule,
    AppBreadcrumbModule.forRoot(),
    AppFooterModule,
    AppHeaderModule,
    AppSidebarModule,
    PerfectScrollbarModule,
    HttpClientModule,
    SlimLoadingBarModule,
    TranslateModule.forRoot({
      loader: {
          provide: TranslateLoader,
          useFactory: HttpLoaderFactory,
          deps: [HttpClient]
      }
    }),
    ChartsModule,
    MatSnackBarModule
  ],
  declarations: [
    AppComponent,
    ...APP_CONTAINERS,
    HrefPreventDefaultDirective,
    P404Component,
    FieldErrorDisplayComponent
  ],
  exports: [HrefPreventDefaultDirective],
  providers: [DetailRowService,
    { provide: APP_BASE_HREF, useValue: '/grp-portal'},
    { provide: MAT_DATE_LOCALE, useValue: 'bn-BD'},
    { provide: ErrorHandler, useClass: GlobalErrorHandler },
    { provide: HTTP_INTERCEPTORS,  useClass: HttpErrorInterceptor,   multi: true  },
    { provide: HTTP_INTERCEPTORS,  useClass: LoadingInterceptor,   multi: true  },
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
